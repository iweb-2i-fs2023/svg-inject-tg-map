SVGInject.setOptions({ makeIdsUnique: false });
const elMapTg = document.getElementById('tg-map');
await SVGInject(elMapTg);

const elBzell = document.getElementById('4471');
elBzell.addEventListener('click', (event) => {
  const elTooltip = document.getElementById('tooltip-map');
  elTooltip.classList.remove('do-not-display');
  elTooltip.innerHTML = 'Bischofszell &#128150;';
  elTooltip.style.top = `${event.pageY}px`;
  elTooltip.style.left = `${event.pageX}px`;
});
